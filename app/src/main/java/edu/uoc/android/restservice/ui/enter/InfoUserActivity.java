package edu.uoc.android.restservice.ui.enter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import edu.uoc.android.restservice.rest.model.Followers;
import edu.uoc.android.restservice.rest.model.Owner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoUserActivity extends AppCompatActivity {

    ArrayList<Owner> listaFollowers;
    RecyclerView recyclerViewFollowers;

    TextView textViewRepositories, textViewFollowing, textUser, textId;
    ImageView imageViewProfile;

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_user);

        textViewFollowing = findViewById(R.id.textViewFollowing);
        textViewRepositories = findViewById(R.id.textViewRepositories);
        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);
        textUser = (TextView) findViewById(R.id.user);
        textId = (TextView) findViewById(R.id.numId);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        listaFollowers = new ArrayList<>();
        recyclerViewFollowers = (RecyclerView)findViewById(R.id.recyclerViewFollowers);

        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));

        String loginName = getIntent().getStringExtra("loginName");

        initProgressBar();  // Solo se inicia el progressBar

        mostrarDatosBasicos(loginName);
        mostrarSeguidores(loginName);  //Inicia el metodo creado para los seguidores
    }

    TextView labelFollowing, labelRepositories, labelFollowers, labelNombre, labelId;
    private void initProgressBar()
    {
        progressBar.setVisibility(View.VISIBLE);
        textViewFollowing.setVisibility(View.INVISIBLE);
        textViewRepositories.setVisibility(View.INVISIBLE);
        imageViewProfile.setVisibility(View.INVISIBLE);
        recyclerViewFollowers.setVisibility(View.INVISIBLE);
        textUser.setVisibility(View.INVISIBLE);
        textId.setVisibility(View.INVISIBLE);

        labelId = (TextView) findViewById(R.id.id);
        labelId.setVisibility(View.INVISIBLE);

        labelNombre = (TextView) findViewById(R.id.nombre);
        labelNombre.setVisibility(View.INVISIBLE);

        labelFollowing = (TextView)findViewById(R.id.labelFollowing);
        labelFollowing.setVisibility(View.INVISIBLE);

        labelRepositories = (TextView) findViewById(R.id.labelRepositories);
        labelRepositories.setVisibility(View.INVISIBLE);

        labelFollowers = (TextView) findViewById(R.id.labelFollowers);
        labelFollowers.setVisibility(View.INVISIBLE);

    }

    private void mostrarDatosBasicos(String loginName){
        GitHubAdapter adapter = new GitHubAdapter();
        Call<Owner> call = adapter.getOwner(loginName);
        call.enqueue(new Callback<Owner>() {

            @Override
            public void onResponse(Call<Owner> call, Response<Owner> response) {
                Owner owner = response.body();
                if(owner != null) {
                    textUser.setText(owner.getName().toString());
                    textId.setText(owner.getId().toString());
                    textViewRepositories.setText(owner.getPublicRepos().toString());  //Muestra el numero de repositorios que tiene
                    textViewFollowing.setText(owner.getFollowing().toString());     //Muestra el numero de personas que esta siguiendo
                    Picasso.get().load(owner.getAvatarUrl()).into(imageViewProfile);    //Muestra la imagen de perfil del usuario
                    progressBar.setVisibility(View.GONE);       //Oculta el progressBar
                    textViewFollowing.setVisibility(View.VISIBLE);      //Muestra numero following
                    textViewRepositories.setVisibility(View.VISIBLE);      //Muestra numero repositories
                    imageViewProfile.setVisibility(View.VISIBLE);       //Muestra imagen
                    recyclerViewFollowers.setVisibility(View.VISIBLE);      //Muestra recyclerView Followers
                    labelFollowing.setVisibility(View.VISIBLE);     //Muestra texto Siguiendo
                    labelRepositories.setVisibility(View.VISIBLE);  //Muestra texto repositorios
                    labelFollowers.setVisibility(View.VISIBLE);     //Muestra texto Seguidores
                    labelNombre.setVisibility(View.VISIBLE);        //Muestra texto Nombre
                    textUser.setVisibility(View.VISIBLE);       //Muestra el nombre del usuario
                    labelId.setVisibility(View.VISIBLE);        //Muestra el texto ID
                    textId.setVisibility(View.VISIBLE);     //Muestra los numero de ID del usuario

                } else {
                    progressBar.setVisibility(View.VISIBLE); //Muestra progressBar en caso que no encuentre ningun usuario o no exista el usuario
                    Toast.makeText(InfoUserActivity.this, "No se encontraron resultados", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Owner> call, Throwable t) {

            }
        });
    }

    private void mostrarSeguidores(String loginName){  //Creo un nuevo metodo para los seguidores
        Call<List<Followers>> callFollowers = new GitHubAdapter().getOwnerFollowers(loginName);
        callFollowers.enqueue(new Callback<List<Followers>>(){
            @Override
            public void onResponse(Call<List<Followers>> call, Response<List<Followers>> response) {
                List<Followers> list = response.body(); // Se va a guardar los resultados en una lista
                if (list != null) {
                    // Se envía la lista al adaptador de seguidores para mostrar los datos
                    AdaptadorFollowers adapter = new AdaptadorFollowers(list);
                    recyclerViewFollowers.setAdapter(adapter);
                } else {
                    //Si no encuentra seguidores, esto se encadena con la informacion del usuario si esta registrado o no
                    Toast.makeText(InfoUserActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Followers>> call, Throwable t) {
                //Si no se conecta a la api de los seguidores se mostrara el mensaje
                Toast.makeText(InfoUserActivity.this, "Ha ocurrido un error al realizar la petición", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
